# Lab Session 3 - 29 December 2020

## Demo
1. Write a C program to read an integer number 'n' and display the remainder when 'n' is divided by 4 using 
    * if statements only
    * if-else statements only
    * Nested if-else statements only
    * Switch case Statements only

## Exercise
1. Write a program to read the month(as a number) and print the number of days in the month using switch case statements.

## Additional Exercise
1. Write a program to read the year and print whether it is a leap year or not.
2. Write a program to read two integers(a and b) and compare them. The output should be as follows:
    * a = b : Equal
    * a > b : Greater
    * a < b : Less
3. Write a program to read the marks and print the equivalent grade based on the following criteria
    * >=90 : S
    * >=80 and <90 : A
    * >=70 and <80 : B
    * >=60 and <70 : C
    * >=50 and <60 : D
    * >=40 and <50 : E
    * <40 : F
