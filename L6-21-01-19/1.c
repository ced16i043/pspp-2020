#include<stdio.h>
int count_digits(int num){
    int i,count=0;
    while(num!=0){
        count+=1;
        num/=10;
    }
    return count;
}

int main()
{
    int count,num;
    printf("Enter the number:");
    scanf("%d",&num);
    count = count_digits(num);
    printf("The number of digits in %d are %d.\n",num,count);
    return 0;
}
