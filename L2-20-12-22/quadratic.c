#include<stdio.h>
#include<math.h>

int main() {
    double a, b, c, discriminant, root1, root2;
    printf("Enter coefficients a, b and c: ");
    // Read Co-efficients
    scanf("%lf %lf %lf", &a, &b, &c);

    // Calculate Discriminant
    discriminant = b * b - 4 * a * c;

    // Calculate Roots assuming inputs are correct and det > 0

    root1 = (-b + sqrt(discriminant)) / (2 * a);
    root2 = (-b - sqrt(discriminant)) / (2 * a);
    printf("root1 = %.2lf and root2 = %.2lf\n", root1, root2);

    return 0;
} 
