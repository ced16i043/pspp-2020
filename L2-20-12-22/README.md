# Lab Session 2 - 22 December 2020

## Demo
1. Write a program to calculate the roots of a quadratic equation. Read the coefficients from STDIN and assume that the determinant is always non zero.

## Exercise
1. Write a C program to convert Farenheit to celsius
2. Write a C program to read the value in dollar and prints the corresponding value in rupees
3. Write a C program to read the value in rupees and prints the corresponding value in dollar
4. Write a C program to read the quantity in feet and prints in kilometer
5. Write a C program to read two integers m and n and print quotient and remainder when m/n
