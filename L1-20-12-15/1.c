#include <stdio.h>

int main()
{
    int a,b,sum,product;
    printf("Enter A:");
    // Read A
    scanf("%d",&a);
    printf("Enter B:");
    // Read B
    scanf("%d",&b);
    // Compute Sum
    sum = a + b;
    // Compute Product
    product = a * b;
    //Print results
    printf("Sum: %d\nProduct:%d\n",sum,product);
    return 0;
}
