#include <stdio.h>
#include <math.h>

int main()
{
    int x = (pow(2,31)*(-1))+1;
    // Print X
    printf("Value: %d  Hex: %08x\n",x,x);
    // Decrement 1
    x = x - 1; 
    printf("Subtract 1\nValue: %d  Hex: %08x\n",x,x);
    // Underflow
    x = x - 1; 
    printf("Subtract 1\nValue: %d Hex: %08x\n",x,x);
    return 1;
}
