# Lab Session 1 - 15 December 2020

## Demo
1. Write a c program to read two integers and display the sum and multiplication.
2. Overflow of integers. 
3. Underflow of integers.

## Exercise
1. Write a c program to read two integers and display the subtraction and division.
2. Simulate overflow.
3. Simulate underflow.
