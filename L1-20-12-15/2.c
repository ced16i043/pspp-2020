#include <stdio.h>
#include <math.h>

int main()
{
    int x = pow(2,31)-2;
    // Print X
    printf("Value: %d  Hex: %08x\n",x,x);
    // Increment 1
    x = x + 1; 
    printf("Add 1\nValue: %d  Hex: %08x\n",x,x);
    // Overflow
    x = x + 1; 
    printf("Add 1\nValue: %d Hex: %08x\n",x,x);
    return 0;
}
