# Lab Session 4 - 02 January 2021

## Demo
1. Write a C program to read n integers into an array and find mean and standard deviation.

## Exercise
1. Write a C program to read n integers into an array and add only the odd numbers in the array.
2. Write a C program to read n integers into an array and check if the array is sorted in ascending order.

## Additional Exercise
1. Write a C program to read an integer n and calculate its factorial(n!).
2. Write a C program to read an integer n and print the number of digits in the number.
3. Write a C program to read a number and print its digits in the reverse order.
4. Write a C program to read an array and print the array in reverse.

