#include<stdio.h>
#include<math.h>

int main()
{
    int i, n, arr[100];
    long double sum, mean, stdev;
    printf("Enter n(<=100):");
    scanf("%d",&n);
    if(n>100){
        printf("Array size too large.\n");
        return 0;
    }
    printf("Enter the elements in the array one by one:\n");
    for(i=0;i<n;++i)
        scanf("%d",&arr[i]);
    for(sum=0,i=0;i<n;++i)
        sum+=arr[i];
    mean = sum/n;
    for(sum=0,i=0;i<n;i+=1)
        sum+=(arr[i]-mean)*(arr[i]-mean);
    stdev=sqrt(sum/n);
    printf("Mean:%Lf\n",mean);
    printf("Standard Deviation:%Lf\n",stdev);
    return 0;
}
