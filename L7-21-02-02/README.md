# Lab Session 7 - 02 February 2021

## Demo

1. Write a recursive function to add n numbers.

##Exercise 

1. Write a recursive function to multiply n numbers.
2. Write a C program to print an array in reverse order using recursive function.

## Extra problem:
1. Write a recursive function to compute nCr.
**Note**: Formula to compute `nCr: nCr = n! / (n – r)! r!`
