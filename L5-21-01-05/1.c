#include<stdio.h>
#include<math.h>

int main()
{
    int i, n, j, dup;
    long long sum;
    printf("Enter n(<=100):");
    scanf("%d",&n);
    if(n>100){
        printf("Array size too large.\n");
        return 0;
    }
    int arr[n];
    printf("Enter the elements(each element should lie withing [1,n-1]) in the array one by one:\n");
    for(i=0;i<n;++i){
        scanf("%d",&arr[i]);
    }
    for(i=0;i<n-1;++i){
        for(j=i+1;j<n;++j){
            if(arr[i]==arr[j]){
                dup = arr[i];
                printf("Duplicate number in the series is %d.\n",dup);
            }
        }
    }
    sum = (n * (n-1) / 2) + dup;
    printf("Sum of the array is %lld.\n",sum);
    return 0;
}
