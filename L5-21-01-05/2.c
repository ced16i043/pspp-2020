#include<stdio.h>
#include<math.h>

int main()
{
    int i, n, j, dup;
    long long sum=0;
    printf("Enter n(<=100):");
    scanf("%d",&n);
    if(n>100){
        printf("Array size too large.\n");
        return 0;
    }
    int arr[n];
    printf("Enter the elements(each element should lie withing [1,n-1]) in the array one by one:\n");
    for(i=0;i<n;++i){
        scanf("%d",&arr[i]);
        sum = sum + arr[i];
    }
    sum = sum - (n * (n-1) / 2);
    printf("The duplicate element is %lld.\n",sum);
    return 0;
}
