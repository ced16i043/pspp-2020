# Lab Session 5 - 05 January 2021

## Demo
1. Write a C program that reads n integers from the range 1 to n-1(n-1 are distinct) and find the duplicate number. Find the sum of integers in the array and use the formula n\*(n-1)/2).

## Exercise
1. Write a C program that reads an array of n integers and also new integer x check if there exist a and b in the array such that a+b=x.
2. Write a C program to implement the bubble sort for sorting in descending order and also print the number of comparison and swaps(comparisons of the array elements only to be considered).


## Additional Exercise
1. Write a C program to read an integer n and find the sum of all odd numbers upto n.
2. Write a C program to read a string and check whether its a pallindrome or not.
3. Write a C program to read a number and calculate the sum of its digits.
4. Write a C program to read an integer n and print all numbers from n to 1.

