#include<stdio.h>
#include<stdlib.h>

void call_by_value(int x, char y, float z, int *a)
{
    printf("[Value] Int: %d, Char: %c, Float: %f, Array: %d,%d\n",x,y,z,a[0],a[1]);
    x = x + 1;
    y = y + 1;
    z = z + 1;
    int temp = a[0];
    a[0] = a[1];
    a[1] = temp;
    printf("[Value] Int: %d, Char: %c, Float: %f, Array: %d,%d\n",x,y,z,a[0],a[1]);
    return;
}

void call_by_reference(int *x, char *y, float *z, int *a)
{
    printf("[Reference] Int: %d, Char: %c, Float: %f, Array: %d,%d\n",*x,*y,*z,a[0],a[1]);
    *x = *x + 1;
    *y = *y + 1;
    z[0] = *z + 1;
    int temp = a[0];
    a[0] = a[1];
    a[1] = temp;
    printf("[Reference] Int: %d, Char: %c, Float: %f, Array: %d,%d\n",*x,*y,*z,a[0],a[1]);
    return;
}

void main()
{
    int x;
    char y;
    float z;
    int *a = (int *)malloc(sizeof(int)*2);
    printf("[Main] Enter an int, char, float and an array of 2 integers:\n");
    scanf("%d %c %f %d %d", &x, &y, &z, &a[0], &a[1]);
    printf("[Main] Values Int: %d, Char: %c, Float: %f, Array: %d,%d\n",x,y,z,a[0],a[1]);
    printf("[Main] Address Int: %x, Char: %x, Float: %x, Array: %x\n",&x,&y,&z,a);
    call_by_value(x,y,z,a);
    printf("[Main] Values Int: %d, Char: %c, Float: %f, Array: %d,%d\n",x,y,z,a[0],a[1]);
    call_by_reference(&x,&y,&z,a);
    printf("[Main] Values Int: %d, Char: %c, Float: %f, Array: %d,%d\n",x,y,z,a[0],a[1]);
}

