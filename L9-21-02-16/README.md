# Lab Session 9 - 16 February 2021

## Demo

1. In main, Read int, char, float and array of integers using x, y,z and  a respectively. Print their values and addresses in main. Then  
    * call function by value update variables in the function, and print them in main 
    * call the function by address and modify them  and print them in main

##Exercise 
1. Write a c function to implement bubble sort without using array(use pointer to integer, and also use malloc function as `int* a = (int *) malloc(sizeof(int))*100)`. call the function from main after reading the sequence to sort. After function call,  print the sorted sequence in main function.
